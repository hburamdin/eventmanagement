﻿using EventManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventManagementSystem.Controllers
{
    public class EventController : Controller
    {
        readonly IUnitOfModel model;

        public EventController()
        {
            model = SingletonOfModel.Instance;
        }

        public EventController(IUnitOfModel unitOfModel)
        {
            model = unitOfModel;
        }

        public ActionResult ApproveEvents()
        {
            var eventsVM = new EventsApproveViewModel(model);
            return View("Admin", "_Layout", eventsVM.EventData);
        }

        // GET: Event
        public ActionResult Index()
        {
            var roleName = Convert.ToString(Session["UserRole"]);
            if (roleName.Equals("Admin"))
            {
                var eventsVM = new EventsViewModel(model, 0);
                return View("Host", "_Layout", eventsVM.EventData);
            }
            else if (roleName.Equals("Host"))
            {
                var eventsVM = new EventsViewModel(model, Convert.ToInt16(Session["UserId"]));
                return View("Host", "_Layout", eventsVM.EventData);
            }
            else
            {
                var eventsVM = new EventsViewModel(model, Convert.ToInt16(Session["UserId"]));
                return View("Index", "_Layout", eventsVM.EventData);
            }
        }

        //[HttpPost]
        //public ActionResult MultipleCommand(EventDataUI eventUI, string command)
        //{
        //    if (command.ToLower().Equals("new"))
        //        return Create;
        //    if (command.ToLower().Equals("sub"))
        //        return Subscribe();

        //    return Index();
        //}
        public ActionResult Subscribe(int eventId)
        {
            if (eventId > 0)
            {
                var userId = Convert.ToInt16(Session["UserId"]);
                model.EventSubsRepo.Add(new EventSubscriptionEntity()
                { EventId = eventId, UserId = userId });
                model.Commit();

                ViewBag.result = Constants.EventSubscribed;
            }

            return Index();
        }

        public ActionResult Approve(int eventId, int userId)
        {
            var evs = model.EventSubsRepo.GetAll().FirstOrDefault(s => s.EventId == eventId
                        && s.UserId == userId);
            evs.IsApproved = true;
            model.Commit();
            ViewBag.result = Constants.EventApproved;

            return Index();
        }

        public ActionResult ViewAttendees(int eventId)
        {
            if (eventId > 0)
            {
                var vmModel = from m in model.EventsRepo.ViewAttendees(eventId)
                              select new UserViewModel { UserName = m };

                return View("ViewAttendees", "_Layout", vmModel);
            }

            return View();
        }

        public ActionResult Create()
        {
            var eventsVM = new EventEntityViewModel(model);
            return View("Create", "_Layout", eventsVM);
        }

        [HttpPost]
        public ActionResult CreateEvent(EventEntityViewModel eventVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.EventsRepo.Add(eventVM.EventEntity);
                    model.Commit();
                    ViewBag.result = Constants.EventCreated;
                }
            }
            catch (DataException ex)
            {
                ModelState.AddModelError("", "Unable to save changes.");
            }

            return RedirectToAction("Create");
        }
    }
}