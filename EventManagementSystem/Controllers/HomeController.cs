﻿using EventManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventManagementSystem.Controllers
{
    public class HomeController : Controller
    {
        readonly IUnitOfModel model;

        public HomeController()
        {
            model = SingletonOfModel.Instance;
        }

        public HomeController(IUnitOfModel unitOfModel)
        {
            model = unitOfModel;
        }

        public ActionResult Index()
        {
            return View("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            var vm = new UserViewModel();
            return View(vm);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Index");
        }


        [HttpPost]
        public ActionResult ValidateLogin(UserViewModel user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userId = model.UsersRepo.GetUserId(user.UserName, user.Password);
                    if (userId != null)
                    {
                        Session["UserId"] = userId;
                        Session["UserName"] = user.UserName;
                        Session["UserRole"] = model.GetRole(userId.Value);
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ModelState.AddModelError("user.UserName", Constants.LoginFail);
                        return View("Login", user);
                    }
                }
                else
                {
                    ModelState.AddModelError("user.UserName", Constants.LoginFail);
                    return View("Login", user);
                }
            }
            catch (DataException ex)
            {
                ModelState.AddModelError("user.UserName", Constants.SaveError);
            }

            return View("Index");
        }

        public ActionResult Register()
        {
            var usersVM = new UserEntityRegisterVM(model);
            return View(usersVM);
        }

        [HttpPost]
        public ActionResult RegisterUser(UserEntity user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var isExists = model.UsersRepo.IsExists(user.UserName);
                    if (isExists)
                    {
                        ModelState.AddModelError("user.UserName", Constants.UserExists);
                        var usersVM = new UserEntityRegisterVM(model);
                        usersVM.User = user;
                        return View("Register",usersVM);
                    }
                    else
                    {
                        model.UsersRepo.Add(user);
                        model.Commit();
                        return RedirectToAction("Register");
                    }
                }
            }
            catch (DataException ex)
            {
                ModelState.AddModelError("user.UserName", Constants.SaveError);
            }

            return View("Register");
        }

    }
}