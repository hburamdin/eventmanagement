﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagementSystem.Controllers
{
    public class Constants
    {
        public const string LoginFail = "Fail to login.";
        public const string UserExists = "User already exists.";

        public const string SaveError = "Unable to save changes.";

        public const string EventCreated = "Event created succesfully.";

        public const string EventSubscribed = "Event subscribed succesfully.";

        public const string EventApproved = "Event approved succesfully.";

    }
}