﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventManagementSystem.Models;

namespace EventManagementSystem
{
    public sealed class SingletonOfModel
    {
        SingletonOfModel()
        {
        }
        private static readonly object padlock = new object();
        private static IUnitOfModel instance = null;
        public static IUnitOfModel Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (padlock)
                    {
                        if (instance == null)
                        {
                            instance = new UnitOfModel(System.Configuration.ConfigurationManager.AppSettings["DbContext"]);
                        }
                    }
                }
                return instance;
            }
        }
    }
}