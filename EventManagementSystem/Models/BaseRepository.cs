﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EventManagementSystem.Models
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected EventMgmtContext _dbContext;
        protected IDbSet<TEntity> _dbSet;

        public BaseRepository(EventMgmtContext context)
        {
            this._dbContext = context;
            this._dbSet = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }
        
        public IEnumerable<TEntity> GetAll()
        {
            return _dbSet.AsEnumerable();
        }
    }
}