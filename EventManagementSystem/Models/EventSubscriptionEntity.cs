﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagementSystem.Models
{
    [Table("EventSubscriptions")]
    public class EventSubscriptionEntity
    {
        [Key]
        public int EventSubscriptionId { get; set; }

        public bool IsApproved { get; set; }

        [Required]
        [ForeignKey("Events")]
        public int EventId { get; set; }

        public virtual EventEntity Events { get; set; }

        [Required]
        [ForeignKey("Users")]
        public int UserId { get; set; }
        public virtual UserEntity Users { get; set; }
    }
}