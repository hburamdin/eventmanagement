﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EventManagementSystem.Models
{
    [Table("Events")]
    public class EventEntity 
    {
        [Key]
        public int EventId { get; set; }

        [Required]
        [MaxLength(50)]
        public string EventName { get; set; }

        [Required]
        [ForeignKey("Users")]
        public int HostId { get; set; }
        public virtual UserEntity Users { get; set; }
    }
}