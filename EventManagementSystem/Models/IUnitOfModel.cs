﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EventManagementSystem.Models
{
    public interface IUnitOfModel
    {
        IUserRepository<UserEntity> UsersRepo { get; }

        IRepository<UserRoleEntity> UserRolesRepo { get; }

        IRepositoryEvents<EventEntity> EventsRepo { get; }

        IRepository<EventSubscriptionEntity> EventSubsRepo { get; }


        string GetRole(int userId);

        void Commit();

        DbContext Context { get; }
    }

    public class EventRepository<TEntity> : BaseRepository<TEntity>, IRepositoryEvents<TEntity> where TEntity : class
    {
        public EventRepository(EventMgmtContext context) : base(context)
        {

        }

        public IEnumerable<string> ViewAttendees(int eventId)
        {
            return from e in _dbContext.EventSubscriptions
                   join v in _dbContext.Users on e.UserId equals v.UserId
                   where e.EventId == eventId
                   select v.UserName;
        }

        public IEnumerable<EventDataUI> ViewEvents()
        {
            return from ev in _dbContext.Events
                   join usr in _dbContext.Users on ev.HostId equals usr.UserId
                   select new EventDataUI
                   {
                       EventName = ev.EventName,
                       HostName = usr.UserName,
                       EventId = ev.EventId,
                       UserId = usr.UserId
                   };
        }

        public IEnumerable<EventDataUI> ViewSubscribedEvents(int userId)
        {
            return from ev in _dbContext.Events
                   join usr in _dbContext.Users on ev.HostId equals usr.UserId
                   join evs in _dbContext.EventSubscriptions on ev.EventId equals evs.EventId
                   into ps
                   from evs in ps.DefaultIfEmpty()
                   select new EventDataUI
                   {
                       EventName = ev.EventName,
                       HostName = usr.UserName,
                       EventId = ev.EventId,
                       UserId = usr.UserId,
                       IsSubscribed = evs != null && evs.UserId == userId
                   };
        }

        public IEnumerable<EventDataUI> ViewApprovalEvents()
        {
            return from evs in _dbContext.EventSubscriptions
                   join usr in _dbContext.Users on evs.UserId equals usr.UserId
                   join ev in _dbContext.Events on evs.EventId equals ev.EventId
                   select new EventDataUI
                   {
                       EventName = ev.EventName,
                       HostName = usr.UserName,
                       EventId = evs.EventId,
                       UserId = evs.UserId,
                       IsApproved = evs.IsApproved
                   };
        }
    }
}