﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EventManagementSystem.Models
{
    public interface IEventMgmtContext
    {
        IDbSet<UserEntity> Users { get; set; }

        IDbSet<UserRoleEntity> UserRoles { get; set; }

        void CommitChanges();
    }
}