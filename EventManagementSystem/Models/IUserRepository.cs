﻿namespace EventManagementSystem.Models
{
    public interface IUserRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        int? GetUserId(string userName, string password);
        bool IsExists(string userName);

    }
}