﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EventManagementSystem.Models
{
    public class UnitOfModel : IUnitOfModel
    {
        private readonly EventMgmtContext _dbContext;
        private UserRepository<UserEntity> _users;
        private BaseRepository<UserRoleEntity> _userRoles;
        private EventRepository<EventEntity> _events;
        private BaseRepository<EventSubscriptionEntity> _eventSubs;

        public UnitOfModel(string conn)
        {
            _dbContext = new EventMgmtContext(conn);
        }

        public IUserRepository<UserEntity> UsersRepo
        {
            get
            {
                return _users ?? (_users = new UserRepository<UserEntity>(_dbContext));
            }
        }

        public IRepository<UserRoleEntity> UserRolesRepo
        {
            get
            {
                return _userRoles ?? (_userRoles = new BaseRepository<UserRoleEntity>(_dbContext));
            }
        }

        public IRepositoryEvents<EventEntity> EventsRepo
        {
            get
            {
                return _events ?? (_events = new EventRepository<EventEntity>(_dbContext));
            }
        }

        public IRepository<EventSubscriptionEntity> EventSubsRepo
        {
            get
            {
                return _eventSubs ?? (_eventSubs = new BaseRepository<EventSubscriptionEntity>(_dbContext));
            }
        }

        public DbContext Context => _dbContext;

        public string GetRole(int userId)
        {
            var res = from u in UsersRepo.GetAll()
                      join r in UserRolesRepo.GetAll()
                       on u.UserRoleId equals r.UserRoleId
                      where u.UserId == userId
                      select r;

            return res != null ? res.FirstOrDefault().RoleName : string.Empty;
        }

        public void Commit()
        {
            _dbContext.SaveChanges();
        }
    }
}