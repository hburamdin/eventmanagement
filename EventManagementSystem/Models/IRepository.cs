﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventManagementSystem.Models
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);
        IEnumerable<TEntity> GetAll();

    }

    public interface IRepositoryEvents<TEntity> : IRepository<TEntity> where TEntity : class
    {
        IEnumerable<string> ViewAttendees(int eventId);

        IEnumerable<EventDataUI> ViewSubscribedEvents(int userId);

        IEnumerable<EventDataUI> ViewEvents();

        IEnumerable<EventDataUI> ViewApprovalEvents();
    }
}
