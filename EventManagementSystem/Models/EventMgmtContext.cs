﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace EventManagementSystem.Models
{
    public class EventMgmtContext : DbContext, IEventMgmtContext
    {
        public EventMgmtContext()
        {
                
        }
        public EventMgmtContext(string conn) : base(conn)
        {
            Database.SetInitializer(new MigrateInitializer(conn));
        }

        public IDbSet<UserEntity> Users { get; set; }

        public IDbSet<UserRoleEntity> UserRoles { get; set; }

        public IDbSet<EventEntity> Events { get; set; }

        public IDbSet<EventSubscriptionEntity> EventSubscriptions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Configure default schema
            modelBuilder.HasDefaultSchema("dbo");

            modelBuilder.Entity<EventSubscriptionEntity>()
            .HasRequired(c => c.Events)
            .WithMany()
            .WillCascadeOnDelete(false);

            modelBuilder.Entity<EventSubscriptionEntity>()
            .HasRequired(c => c.Users)
            .WithMany()
            .WillCascadeOnDelete(false);
        }

        void IEventMgmtContext.CommitChanges()
        {
            this.SaveChanges();
        }
    }

    internal sealed class DbConfiguration : DbMigrationsConfiguration<EventMgmtContext>
    {
        public DbConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "EventManagementSystem.Models.EventMgmtContext";
        }

        protected override void Seed(EventMgmtContext context)
        {
            if (context.UserRoles.Count() == 0)
            {
                context.UserRoles.Add(new UserRoleEntity() { UserRoleId = 1, RoleName = "Admin" });
                context.UserRoles.Add(new UserRoleEntity() { UserRoleId = 2, RoleName = "Host" });
                context.UserRoles.Add(new UserRoleEntity() { UserRoleId = 3, RoleName = "Attendee" });
                context.SaveChanges();

                context.Users.Add(new UserEntity() { UserId = 1, UserName = "Admin", Password = "Admin", Email = "admin@gmail.com", UserRoleId = 1 });
                context.SaveChanges();
            }
        }
    }

    internal class MigrateInitializer : MigrateDatabaseToLatestVersion<EventMgmtContext, DbConfiguration>
    {
        public MigrateInitializer(string connectionString) : 
            base(true, new DbConfiguration()
            { TargetDatabase = new DbConnectionInfo(connectionString, "System.Data.SqlClient") })
        {
        }
    }
}