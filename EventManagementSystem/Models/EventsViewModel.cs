﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagementSystem.Models
{
    public class EventEntityViewModel
    {
        public EventEntity EventEntity { get; set; }

        public IEnumerable<UserEntity> Users { get; set; }

        public EventEntityViewModel()
        {

        }

        public EventEntityViewModel(IUnitOfModel model)
        {
            EventEntity = new EventEntity();
            Users = from u in model.UsersRepo.GetAll()
                    join r in model.UserRolesRepo.GetAll() on u.UserRoleId equals r.UserRoleId
                    where r.RoleName.Equals("Host")
                    select u;
        }
    }

    public class EventDataUI
    {
        public EventDataUI()
        {

        }
        public int EventId { get; set; }
        public int UserId { get; set; }

        public bool IsSubscribed { get; set; }

        public string EventName { get; set; }

        public string HostName { get; set; }

        public string UserName { get; set; }

        public bool IsApproved { get; set; }
    }

    public class EventsViewModel
    {
        public IEnumerable<EventDataUI> EventData { get; set; }

        public EventsViewModel()
        {

        }

        public EventsViewModel(IUnitOfModel model, int userId = 0)
        {
            if (userId > 0)
                EventData = model.EventsRepo.ViewSubscribedEvents(userId);
            else
                EventData = model.EventsRepo.ViewEvents();
        }
    }

    public class EventsApproveViewModel
    {
        public IEnumerable<EventDataUI> EventData { get; set; }

        public EventsApproveViewModel(IUnitOfModel model)
        {
            EventData = model.EventsRepo.ViewApprovalEvents();
        }
    }
}