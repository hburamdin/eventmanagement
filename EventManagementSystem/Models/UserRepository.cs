﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagementSystem.Models
{
    public class UserRepository<TEntity> : BaseRepository<TEntity>, IUserRepository<TEntity> where TEntity : class
    {
        public UserRepository(EventMgmtContext context) : base(context)
        {

        }

        public int? GetUserId(string userName, string password)
        {
            var users = _dbSet.OfType<UserEntity>().FirstOrDefault(s => s.UserName == userName && s.Password == password);
            if (users != null)
                return users.UserId;

            return null;
        }

        public bool IsExists(string userName)
        {
            return _dbSet.OfType<UserEntity>().Any(s => s.UserName == userName);
        }
    }
}