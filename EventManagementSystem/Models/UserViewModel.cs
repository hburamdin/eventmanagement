﻿using EventManagementSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EventManagementSystem.Models
{
    public class UserEntityRegisterVM
    {
        public UserEntityRegisterVM(IUnitOfModel model)
        {
            Roles = model.UserRolesRepo.GetAll();
        }

        public UserEntity User { get; set; }

        public IEnumerable<UserRoleEntity> Roles { get; set; }
    }

    public class UserViewModel
    {
        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
    }
}