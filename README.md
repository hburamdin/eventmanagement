Prerequisites
1. SQL Server
2. .Net Framework 4.6
3. IIS

Default Data Creation
1. Roles Created as part of EF
	Admin
	Host
	Attendee

2. Admin user created as part of EF.
	Credentials:
	UserName: admin
	Password: admin
	Role: Admin
3. Default Data base will be created
   "Data Source=(localdb)\mssqllocaldb;Initial Catalog=DbContext;Integrated Security=True;MultipleActiveResultSets=True".
   Or define the data base under config in setting "DbContext"