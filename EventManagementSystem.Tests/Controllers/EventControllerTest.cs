﻿using EventManagementSystem.Controllers;
using EventManagementSystem.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Linq;

namespace EventManagementSystem.Tests.Controllers
{
    [TestClass]
    public class EventControllerTest
    {
        IUnitOfModel model;

        [TestCleanup]
        public void Clean()
        {
            model.Context.Database.Delete();
        }

        [TestInitialize]
        public void Init()
        {
            model = new UnitOfModel("EM_" + Guid.NewGuid());
        }

        [TestMethod]
        public void Create()
        {
            try
            {
                // Arrange
                var controller = BuildEventViewSetup(1, "Admin");

                model.UsersRepo.Add(new UserEntity() { UserName = "A1", UserRoleId = 1, Password = "h1", Email = "h1@gmail.com" });
                var viewModel = new EventEntityViewModel(model);
                viewModel.EventEntity = new EventEntity() { EventName = "E1", HostId = 1 };

                // Act
                controller.CreateEvent(viewModel);

                // Assert
                Assert.AreEqual(1, model.EventsRepo.GetAll().Count());
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
          
        }

        [TestMethod]
        public void Events()
        {
            try
            {
                // Arrange
                var controller = BuildEventViewSetup(1, "Admin");

                model.UsersRepo.Add(new UserEntity() { UserName = "A1", UserRoleId = 1, Password = "h1", Email = "h1@gmail.com" });
                var vm = new EventEntityViewModel(model);
                vm.EventEntity = new EventEntity() { EventName = "E1", HostId = 1 };

                // Act
                controller.CreateEvent(vm);
                // Act
                controller = BuildEventViewSetup(2, "Host");
                ViewResult result = controller.Index() as ViewResult;

                // Assert
                var viewModel = result.Model as IEnumerable<EventDataUI>;
                Assert.IsNotNull(result);
                Assert.IsNotNull(viewModel);
                Assert.AreEqual(1, viewModel.Count());
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void Subscribe()
        {
            try
            {
                var controller = BuildEventViewSetup(2, "Attendee");

                model.UsersRepo.Add(new UserEntity() { UserName = "H1", UserRoleId = 2, Password = "h1", Email = "hi@gmail.com" });
                model.UsersRepo.Add(new UserEntity() { UserName = "U1", UserRoleId = 3, Password = "u1", Email = "ui@gmail.com" });

                model.EventsRepo.Add(new EventEntity() { EventName = "E1", HostId = 1 });
                model.Commit();

                // Act
                controller.Subscribe(1);

                // Assert
                Assert.AreEqual(1, model.EventSubsRepo.GetAll().Count());
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void Approve()
        {
            try
            {
                var controller = BuildEventViewSetup(2, "Attendee");

                model.UsersRepo.Add(new UserEntity() { UserName = "H1", UserRoleId = 2, Password = "h1", Email = "hi@gmail.com" });
                model.UsersRepo.Add(new UserEntity() { UserName = "U1", UserRoleId = 3, Password = "u1", Email = "ui@gmail.com" });
                model.UsersRepo.Add(new UserEntity() { UserName = "U2", UserRoleId = 3, Password = "u2", Email = "u2@gmail.com" });

                model.EventsRepo.Add(new EventEntity() { EventName = "E1", HostId = 1 });
                model.Commit();

                // Act
                controller.Subscribe(1);

                controller = BuildEventViewSetup(1, "Admin");

                // Act
                controller.Approve(1, 2);

                // Assert
                Assert.IsTrue(model.EventSubsRepo.GetAll().FirstOrDefault(s => s.UserId == 2).IsApproved);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        private EventController BuildEventViewSetup(int userId, string userRole)
        {
            // Arrange
            var mockControllerContext = new Mock<ControllerContext>();
            var mockSession = new Mock<HttpSessionStateBase>();
            mockSession.SetupGet(s => s["UserRole"]).Returns(userRole); //somevalue
            mockSession.SetupGet(s => s["UserId"]).Returns(userId); //somevalue

            mockControllerContext.Setup(p => p.HttpContext.Session).Returns(mockSession.Object);

            var controller = new EventController(model);
            controller.ControllerContext = mockControllerContext.Object;

            return controller;
        }
    }
}
